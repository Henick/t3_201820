package model.vo;

import java.util.Date;

public class VOStation {
	
	private int id;
	
	private String name;
	
	private String city;
	
	private double latitude;
	
	private double longitude;
	
	private int capacity;
	
	private Date date;
	

	public VOStation(int id, String name) {
		this.id=id;
		this.name=name;
	}
	public VOStation(int pId, String pName, String pCity, double pLatitude, double pLongitude, int pCapacity, Date pDate) {
		id=pId;
		city=pCity;
		latitude=pLatitude;
		longitude=pLongitude;
		capacity=pCapacity;
		date=pDate;
	}
	
	public int darId() {
		return id;
	}
	public String darNombre() {
		return name;
	}
}
