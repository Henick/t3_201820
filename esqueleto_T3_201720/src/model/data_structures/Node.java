package model.data_structures;

public class Node<E> {

	private E elemento;
	
	private Node<E> siguiente;
	
	public Node(E elemento) {
		this.elemento=elemento;
	}
	
	public E darElemento() {
		return elemento;
	}
	
	public void cambiarSiguiente(Node<E> siguiente) {
		this.siguiente=siguiente;
	}
	
	public Node<E> darSiguiente(){
		return siguiente;
	}
}
